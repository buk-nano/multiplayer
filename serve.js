var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, { origins: '*:*'});

app.get('/', function(req, res){
    res.send('<h1>Hello world</h1>');
});

io.on('connection', function(socket){
  console.log('a user connected with id: ' + socket.id);
  socket.broadcast.emit('new', {
    id: socket.id
  });
  socket.on('name', function(name){
    socket.broadcast.emit('name', {
        id: socket.id,
        name: name
    });
  });
  socket.on('x', function(x){
    socket.broadcast.emit('x', {
        id: socket.id,
        x: x
    });
  });
  socket.on('y', function(y){
    socket.broadcast.emit('y', {
        id: socket.id,
        y: y
    });
  });
  socket.on('image', function(image){
    socket.broadcast.emit('image', {
        id: socket.id,
        image: image
    });
  });
  socket.on('direction', function(direction){
    socket.broadcast.emit('direction', {
        id: socket.id,
        direction: direction
    });
  });
  socket.on('disconnect', function () {
    console.log('a user disconnected with id: ' + socket.id);
    socket.broadcast.emit('gone', {
        id: socket.id
    });
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});